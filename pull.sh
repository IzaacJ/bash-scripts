#!/bin/bash
function help {
	echo "Please execute script with like this:"
	echo '    pull.sh [projectdir = current dir [remote = origin [branch = master]]]'
	exit 1
}
if [[ "$@" == "--help" ]] || [[ "$@" == "-h" ]]
	then
		help
fi
if [ -n "$1" ]
then
	project="$1"
	cd "$project" &>/dev/null
	if [ $? -ne 0 ]; then
		echo "$1 does not exist. Please check your path."
		echo "Exiting..."
		exit 1
	fi
else
	project="$PWD"
fi
if [ -z "$2" ]
then
		remote="origin"
else
		remote="$2"
fi
if [ -z "$3" ]
then
		branch="master"
else
		branch="$3"
fi
echo
echo "Pulling $project from $remote:$branch..."
git pull $remote $branch &>/dev/null
error=$?
if [ $error -ne 0 ]
	then
		if [ $error -eq 128 ]
		then
			echo
			echo "$project is not a git repository."
			echo "Make sure you're in the right directory or add the path to your call."
			echo
			help
		fi
		echo "Something went wrong: $error"
		echo "Exiting..."
		exit 0
fi
echo "Updating and pulling any submodules..."
git submodule update --init --recursive &>/dev/null
git submodule foreach git pull origin master &>/dev/null
echo "Pulling complete!"