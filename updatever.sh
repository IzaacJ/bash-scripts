#!/bin/bash
function help {
	echo "Please execute script with like this:"
	echo '    build.sh "DIRECTORY" ["ARCHIVE_NAME"]'
	echo
	echo '    DIRECTORY  	The directory containing the files.'
	echo '    ARCHIVE_NAME 	The name of the buildt archive. USE %TS% to include build date and time.'
	exit 1
}
#if [ -n "$1" ]; then
#	directory="$1"
#	cd "$directory" &>/dev/null
#	if [ $? -ne 0 ]; then
#		echo "$1 does not exist. Please check your path."
#		echo
#		help
#	fi
#	cd .. &>/dev/null
#else
#	help
#fi
#if [ -n "$2" ]; then
#	name="$2"
#	name="${name//%TS%/$TS}"
#else
#	name="$(basename "$1")-$TS" &>/dev/null
#fi
#
#echo "Building Project $(basename "$1")..."
#echo "Archive Name: $name"

sed -r 's/(.*)(\?Version:\s=)([0-9]+)(.*)/echo "\1\2$((\3+1))\4"/ge' test.txt

echo "Complete!"