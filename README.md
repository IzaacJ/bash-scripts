Here I'll put some bash scripts I'm using during development and other stuff.

All scripts are tested in git-bash.

## Installation:
If you're using git bash on Windows, put these in:

```
#!

{USERDIRECTORY}\bin
```

### Usage:
**PUSH.sh**

```
#!bash

push.sh [project_path [commit_message [remote [branch]]]]
```

* **project_path**: The relative or absolute path to the git project. Defaults to current directory.
* **commit_message**: The commit message to be used. If empty or "--auto" it gives you a prompt.
* **remote**: The remote to push to. Defaults to origin.
* **branch**: The branch to push to. Defaults to master.

Example:

* *Absolute path, promted for commit message, remote origin, branch dev*
*  **Current dir**: ~/
*  **Project dir**: /var/www/dev/project
*  **Command**: push.sh /var/www/dev/project --auto origin dev

* *Relative path, included commit message, remote dev, branch master*
*  **Current dir**: /var/www
*  **Project dir**: /var/www/dev/project
*  **Command**: push.sh dev/project "My custom commit message" dev

* *Current directory, promted commit message, remote origin, branch master*
*  **Current dir**: /var/www/dev/project
*  **Project dir**: /var/www/dev/project
*  **Command**: push.sh



**PULL.sh**

```
#!bash

pull.sh [project_path [remote [branch]]]
```

* **project_path**: The relative or absolute path to the git project. Defaults to current directory.
* **remote**: The remote to pull from. Defaults to origin.
* **branch**: The branch to pull from. Defaults to master.

Example:

* *Absolute path, remote origin, branch dev*
*  **Current dir**: ~/
*  **Project dir**: /var/www/dev/project
*  **Command**: pull.sh /var/www/dev/project origin dev

* *Relative path, remote dev, branch master*
*  **Current dir**: /var/www
*  **Project dir**: /var/www/dev/project
*  **Command**: pull.sh dev/project dev

* *Current directory, remote origin, branch master*
*  **Current dir**: /var/www/dev/project
*  **Project dir**: /var/www/dev/project
*  **Command**: pull.sh


**TAG.sh**

```
#!bash

tag.sh TAGNAME [TAGMESSAGE [PROJDIR [REMOTE [COMMIT]]]]
```

* **TAGNAME**: The tag to use. -a, --all to ignore tag and push all tags.
* **TAGMESSAGE**: The message. -i, --ignore to ignore tagging, -a, --auto for promt.
* **PROJDIR**: The relative or absolute path to the git project. -c, --current for current directory.
* **REMOTE**: The remote to push tag to. -i, --ignore to ignore push, -a, --auto for origin.
* **COMMIT**: The commit to tag/push. Exclude to use the latest commit.

Example:

* *coming soon...*