#!/bin/bash
function help {
	echo "Please execute script with like this:"
	echo '    tag.sh TAGNAME ["TAGMESSAGE" [PROJDIR [REMOTE [COMMIT]]]]'
	echo
	echo '    TAGNAME    Tagname to tag and/or push. -a, --all to push all tags.'
	echo '    TAGMESSAGE Tag message or -i, --ignore to ignore tagging, -a, --auto for promt.'
	echo '    PROJDIR    Project path or -c, --current for the current directory.'
	echo '    REMOTE     Remote git or -i, --ignore to ignore push. -a, --auto for origin.'
	echo '    COMMIT     Commit to tag. Exclude to tag the latest commit.'
	exit 1
}
ignore=0
ignorepush=0

if [[ "$@" == "--help" ]] || [[ "$@" == "-h" ]]; then
		help
fi
if [ -n "$1" ]; then
	tag="$1"
else
	help
fi
if [ "$tag" == "--all" ] || [ "$tag" == "-a" ]; then
	ignore=1
fi
if [ -n "$3" ] && [ "$3" != "--current" ] && [ "$3" != "-c" ]; then
	project="$3"
	cd "$project" &>/dev/null
	if [ $? -ne 0 ]; then
		echo "$3 does not exist. Please check your path."
		echo
		help
	fi
else
	project="$PWD"
fi

echo "Project: $PWD"
echo "Tag: $tag"

if [ "$2" != "--ignore" ] && [ "$2" != "-i" ]; then
	if [ -z "$2" ] || [ "$2" == "--auto" ] || [ "$2" == "-a" ]; then 
		date="`date +%Y-%m-%d" "%H:%M:%S`"
		echo "Tag message."
		echo 'Default: "Tag at: '"$date"'"'
		read -p "Tag message: " desc
		if [ -z "$desc" ]; then
			desc="Tag at: $date"
		fi
	else
		desc="$2"
		echo "Tag message: $desc"
	fi
else
	ignore=1
fi
if [ "$4" != "--ignore" ] && [ "$4" != "-i" ]; then
	if [ -z "$4" ] || [ "$4" == "--auto" ] || [ "$4" == "-a" ]; then
			remote="origin"
	else
			remote="$4"
	fi
else
	ignorepush=1
fi
if [ -z "$5" ]; then
		commit=
else
		commit=$4
fi
if [ $ignore -eq 1 ] && [ $ignorepush -eq 1 ]; then
	echo "Tagging and pushing ignored."
	echo
	help
fi
if [ $ignore -eq 0 ]; then
	#git tag -a "$tag" -m "$desc" $commit
	error=$?
	if [ $error -ne 0 ]; then
			if [ $error -eq 128 ]; then
				echo
				echo "$project is not a git repository."
				echo "Make sure you're in the right directory or add the path to your call."
				echo
				help
			fi
			echo "Tagging failed: $?"
			echo
			help
	fi
	echo "Tag $tag applied."
else
	echo "Tagging ignored."
fi
if [ $ignorepush -eq 0 ]; then
	if [ "$tag" == "--all" ] || [ "$tag" == "-a" ]; then
		#git push $remote --tags
		error=$?
		if [ $error -ne 0 ]; then
				if [ $error -eq 128 ]; then
					echo
					echo "$project is not a git repository."
					echo "Make sure you're in the right directory or add the path to your call."
					echo
					help
				fi
				echo "Pushing all tags failed: $?"
				echo
				help
		fi
		echo "Pushed all tags completed!"
	else
		#git push $remote "$tag"
		error=$?
		if [ $error -ne 0 ]; then
				if [ $error -eq 128 ]; then
					echo
					echo "$project is not a git repository."
					echo "Make sure you're in the right directory or add the path to your call."
					echo
					help
				fi
				echo "Pushing tag $tag failed: $?"
				echo
				help
		fi
		echo "Pushed tag ""$tag"" completed!"
	fi
else
	echo "Pushing ignored."
fi
echo "Complete!"