#!/bin/bash
function help {
	echo "Please execute script with like this:"
	echo '    pull.sh [projectdir = current dir ["commit message" [remote = origin [branch = master]]]]'
	exit 1
}
if [[ "$@" == "--help" ]] || [[ "$@" == "-h" ]]; then
		help
fi
if [ -n "$1" ]; then
	project="$1"
	cd "$project" &>/dev/null
	if [ $? -ne 0 ]; then
		echo "$1 does not exist. Please check your path."
		echo
		help
	fi
else
	project="$PWD"
fi
if [ -z "$3" ]; then
		remote="origin"
else
		remote="$3"
fi
if [ -z "$4" ]; then
		branch="master"
else
		branch="$4"
fi
if [ "$2" != "--ignore" ]; then
	if [ -z "$2" ] || [ "$2" == "--auto" ]; then 
		date="`date +%Y-%m-%d" "%H:%M:%S`"
		echo "Project: $PWD"
		echo "Commit message."
		echo 'Default: "Commit at: '"$date"'"'
		read -p "Commit message: " desc
		if [ -z "$desc" ]; then
			desc="Commit at: $date"
		fi
	else
		desc="$2"
	fi
	git add . &>/dev/null
	error=$?
	if [ $error -ne 0 ]; then
		if [ $error -eq 128 ]; then
			echo
			echo "$project is not a git repository."
			echo "Make sure you're in the right directory or add the path to your call."
			echo
			help
		fi
		echo "Unable to add all files."
		echo
		help
	fi
	echo
	echo "Commiting: $desc..."
	git commit -m "$desc"  &>/dev/null
	error=$?
	if [ $error -ne 0 ]; then
		if [ $error -eq 128 ]; then
			echo
			echo "$project is not a git repository."
			echo "Make sure you're in the right directory or add the path to your call."
			echo
			help
		fi
		echo "No changes to commit."
		echo
		help
	fi
fi
echo "Pushing $project to $remote:$branch"
git push $remote $branch
error=$?
if [ $error -ne 0 ]; then
		if [ $error -eq 128 ]; then
			echo
			echo "$project is not a git repository."
			echo "Make sure you're in the right directory or add the path to your call."
			echo
			help
		fi
		echo "Push failed: $?"
		echo
		help
fi
echo "Pushing complete!"